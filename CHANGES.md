### Changelog

#### 1.1.3 [08/15/2019]

* Added hidden text for screen readers / accessibility.
* Additional fixes for new Google icon when displayed in boxes and circles.
* Fix for Yelp icon.

#### 1.1.2 [11/14/2018]

* Google Plus icon now displays as normal Google icon. This change is because Google Plus being retired.
* Added CSS fix that allows Font Awesome icons to display using actual Font Awesome library in social bar rather than built-in outdated version that comes packaged with DMS theme (requires recompile of LESS). This allows use of the most recent Google icon.

#### 1.1.1 [11/21/2014]

* Fixed HTTPS / SSL issues related to loading in non-ssl version of fonts

#### 1.1.0 [11/21/2014]

 * Added support for tooltips
 * Added support for client info tags double curly bracket `{{key}}` syntax for social URL fields. 
 * Fixed z-index issue
 * Fixed sidebar now hides on tablet and mobile

#### 1.0.1 [7/24/2014]

 * Option to enabled social bar on the right side of the window now works.
 * Started working on adding color options.
 * Added example code for adding less vars and less output
 
#### 1.0.0
 
 * Initial release
