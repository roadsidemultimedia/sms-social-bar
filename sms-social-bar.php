<?php
/*
Plugin Name: SMS Social Bar
Plugin URI: https://bitbucket.org/roadsidemultimedia/sms-social-bar/
Description: Add social media icons to the site
Author: Roadside Multimedia
Contributors: Milo Jennings
PageLines: true
Version: 1.1.3
Section: Social Bar
Class Name: SMS_social_bar
Filter: social, component
Loading: active
Bitbucket Plugin URI: https://bitbucket.org/roadsidemultimedia/sms-social-bar/
Bitbucket Branch: master
*/

/**
 * IMPORTANT
 * This tells wordpress to not load the class as DMS will do it later when the main sections API is available.
 * If you want to include PHP earlier like a normal plugin just add it above here.
 */

if( ! class_exists( 'PageLinesSectionFactory' ) )
	return;

class SMS_social_bar extends PageLinesSection {

	function __construct() {
		parent::__construct();

		if( is_ssl() )
			$this->base_url = str_replace( 'http://', 'https://', $this->base_url );

		$url = $this->base_url; // the base url of the section
		$dir = $this->base_dir; // the base directory of the section
		// add_filter( 'pagelines_lesscode', array(&$this,'custom_less_output') );
		add_filter( 'pless_vars', array(&$this,'custom_less_vars') );
		$this->social_tab_counter = 0;
		if( function_exists('client_info_filter') ){
			$this->cit = true;
		}
	}

	//Custom LESS markup
	function custom_less_output($less_output){
		// $less_output .= pl_file_get_contents( dirname( __FILE__ ) . '/sms-social-bar.less' );
		// $less_output .= '.shazbot-7-24{color:#f90; border: 5px dashed #fff;}';
		return $less_output;
	}

	//Custom LESS Vars
	function custom_less_vars($less_vars){
		$less_vars['sms_social_bar_url'] = sprintf( "\"%s\"", $this->base_url );
		// $less_vars['example_url_var'] = sprintf( "\"%s\"", "http://lorempixel.com/50/50/nature/" );
		// $less_vars['example_string_var'] = "superVar9000";
		return $less_vars;
	}

	function section_opts(){

		$the_urls = array();

		$icons = $this->the_icons();

		foreach($icons as $icon){
			if ($icon == 'google-plus') $icon = 'google';
			$the_urls[] = array(
				'label'	=> ui_key($icon) . ' URL',
				'key'	=> 'sl_'.$icon,
				'type'	=> 'text',
				'scope'	=> 'global',
			);
		}

		$opts = array(

			array(
				'type'	=> 'multi',
				'key'	=> 'rsmmsb_config',
				'title'	=> 'Text',
				'col'	=> 1,
				'opts'	=> array(
					array(
						'type'  => 'select',
						'key' => 'rsmmsb_align',
						'label' => 'Alignment & Orientation',
						'opts'  => array(
							'sb-links-right'    => array( 'name' => 'Social links on right'),
							'sb-links-left'     => array( 'name' => 'Social links on left'),
							'sb-links-center'   => array( 'name' => 'Social links centered'),
							'sb-links-vertical' => array( 'name' => 'Social links stacked vertically'),
						),
					),
					array(
						'type'	=> 'select',
						'key'	  => 'rsmmsb_style',
						'label'	=> 'Icon Styles',
						'opts'	=> array(
							'icon-only'	  => array( 'name' => 'Plain icons w/ no background'),
							'box'         => array( 'name' => 'Icons square boxes'),
							'box-rounded' => array( 'name' => 'Icons rounded boxes'),
							'circle'      => array( 'name' => 'Icons in circles'),
						),
					),
					array(
						'type'  => 'check',
						'key'   => 'rsmmsb_full_color',
						'label' => 'Display icons with the colors? If left unchecked, colors will appear on hover instead.',
					),
					array(
						'type'  => 'check',
						'key'   => 'rsmmsb_display_titles',
						'label' => 'Display titles of social networks beside the icons?',
					),
					array(
						'type'  => 'check',
						'key'   => 'rsmmsb_display_tooltips',
						'label' => 'Display tooltips with titles of social networks?',
					),
					array(
						'type'  => 'select',
						'key'   => 'rsmmsb_display_tooltip_position',
						'label' => 'Tooltip Position',
						'opts'	=> array(
							'top'    => array( 'name' => 'Top'),
							'bottom' => array( 'name' => 'Bottom'),
							'left'   => array( 'name' => 'Left'),
							'right'  => array( 'name' => 'Right'),
						),
					),
				)
			),
			array(
				'type'	=> 'multi',
				'key'	=> 'rsmmsb_sidebar_config',
				'title'	=> 'Sidebar Configuration',
				'col'	=> 1,
				'opts'	=> array(
					// Global options
					array(
						'scope' => 'global',
						'type'  => 'check',
						'key'   => 'rsmmsb_social_tab_enable_global',
						'label' => 'Enable social tab attached to the right side of the window.',
					),
					array(
						'scope' => 'global',
						'type'  => 'check',
						'key'   => 'rsmmsb_full_color_global',
						'label' => 'Display icons with the colors? If left unchecked, colors will appear on hover instead.',
					),
					array(
						'scope' => 'global',
						'type'	=> 'select',
						'key'   => 'rsmmsb_style_global',
						'label' => 'Icon Styles',
						'opts'  => array(
							'icon-only'    => array( 'name' => 'Plain icons w/ no background'),
							'box'          => array( 'name' => 'Icons square boxes'),
							'box-rounded'  => array( 'name' => 'Icons rounded boxes'),
							'circle'       => array( 'name' => 'Icons in circles'),
						),
					),
				),
			),
			array(
				'type'	=> 'multi',
				'key'	=> 'sl_urls',
				'title'	=> 'Link URLs',

				'col'	=> 2,
				'opts'	=> $the_urls
			),
				
		);

		return $opts;

	}

	function the_icons( ){

		$icons = array(
			'facebook',
			'linkedin',
			'instagram',
			'twitter',
			'youtube',
			'google-plus',
			'google',
			'pinterest',
			'flickr',
			'yelp',
		);

		return $icons;

	}
	function the_titles( ){

		$titles = array(
			'Facebook',
			'Linkedin',
			'Instagram',
			'Twitter',
			'Youtube',
			'Google',
			'Google',
			'Pinterest',
			'Flickr',
			'Yelp',
		);

		return $titles;

	}

	function section_persistent(){

		if( pl_setting( 'rsmmsb_social_tab_enable_global' ) ){
			add_action('wp_footer', array(&$this, 'add_social_bar_to_footer') );
		}

	}

	function add_social_bar_to_footer(){
		$args = array(
			'global' => true
		);
		echo "<div class='social-sidebar--fixed'>";
		echo $this->section_template($args);
		echo "</div>";
		echo "<script> jQuery(function(){ jQuery('a[rel=tooltip]').tooltip();}); </script>";
	}

	function section_template( $args = false ) {

		$icons = $this->the_icons();

		$titles = $this->the_titles();

		$sites = array_combine($icons, $titles);

		$target = "target='_blank'";

		$align = ( $this->opt('rsmmsb_align') ) ? $this->opt('rsmmsb_align') : 'sb-links-left';

		$display_titles = ( $this->opt('rsmmsb_display_titles') ) ? $this->opt('rsmmsb_display_titles') : false;

		$display_tooltips = ( $this->opt('rsmmsb_display_tooltips') ) ? $this->opt('rsmmsb_display_tooltips') : false;

		if( $args['global'] ){
			$tooltip_position = 'left';
		} else {
			$tooltip_position = ( $this->opt('rsmmsb_display_tooltip_position') ) ? $this->opt('rsmmsb_display_tooltip_position') : 'top';
		}

		if( $args['global'] ){
			$full_color = pl_setting('rsmmsb_full_color_global') ? ' full-color' : ' muted';
		} else {
			$full_color = $this->opt('rsmmsb_full_color') ? ' full-color' : ' muted';
		}

		if( $args['global'] ){
			$icon_style = pl_setting('rsmmsb_style_global') ? pl_setting('rsmmsb_style_global') : ' icon-only';
		} else {
			$icon_style = ( $this->opt('rsmmsb_style') ) ? $this->opt('rsmmsb_style') : ' icon-only';
		}

		if( $icon_style == 'box' || $icon_style == 'box-rounded' || $icon_style == 'circle' ){
			$boxed = ' boxed';
		} else {
			$boxed = '';
		}

	?>
	<div class="rsmm-socialbar-wrap fix <?php echo $align . ' ' ?><?php echo $icon_style ?><?php echo $boxed ?><?php echo $full_color ?>">

		<div class="sb-links">
		<?php

		// <a href="" class="rl-city" target="_blank" ></a>

		foreach($sites as $site => $title){

			$url = ( pl_setting('sl_'.$site) ) ? pl_setting('sl_'.$site) : false;
			
			// Filter through client info tags ,to allow support for {{key}} notation
			if( $url && $this->cit )
				$url = client_info_filter($url);

			$tooltip_title = $title;
			$screen_reader_title = $title;
			$title = ( $display_titles && $args['global'] !== true ) ? "<span class='sb-title'>$title</span>" : '';

			$tooltip_markup = $display_tooltips ? "rel='tooltip' data-original-title='$tooltip_title'  data-placement='$tooltip_position'" : '';
			

			if( $url ) {
				if($site == 'google-plus'){
					printf('<a href="%s" class="sb-link" %s %s><i class="fa fa-google"></i><span class="sr-only">%6$s Business Page, opens in new window</span>%5$s</a>', $url, $target, $tooltip_markup, $site, $title, $screen_reader_title);
				} else {
					printf('<a href="%s" class="sb-link" %s %s><i class="icon icon-%s"></i><span class="sr-only">%6$s Page, opens in new window</span>%s</a>', $url, $target, $tooltip_markup, $site, $title, $screen_reader_title);
				}
			}

		}

		?>
		</div>
	</div>
<?php }

}
