## How to create a release zip

From the plugin folder:

```
cd .. && zip -r sms-social-bar-1.1.3.zip sms-social-bar -x@sms-social-bar/release_excludes.lst
```